{ config, lib, pkgs, inputs, user, ... }:
{
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      systemd-boot = {
        enable = true;
        configurationLimit = 5;
      };
      efi.canTouchEfiVariables = true;
    };
  };

  time.timeZone = "Europe/Copenhagen";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "da_DK.UTF-8";
      LC_MONETARY = "da_DK.UTF-8";
    };
  };

  console = {
    font = "Lat2-Terminus12";
    keyMap = "us";
  };

  services.xserver = {
    enable = true;

    desktopManager = {
      xterm.enable = false;
    };

    displayManager = {
      startx.enable = true;
    };

    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dmenu
        i3status-rust
        i3lock
      ];
    };
  };
  
  services.xserver.layout = "us,dk";

  sound = {
    enable = true;
    mediaKeys.enable = true;
  };
  hardware.pulseaudio.enable = true;

  networking.networkmanager.enable = true;

  services.devmon.enable = true;

  fonts.packages = with pkgs; [
    terminus_font
    terminus_font_ttf
    nerdfonts
    font-awesome
  ];

  environment = {
    systemPackages = with pkgs; [
      binutils
      killall
      lshw
      pciutils
      usbutils
      htop
      tree
      netcat-gnu
      xclip
      arandr
      sysfsutils
      file
      keepassxc
      patchelf_0_13
      ltrace

      jdk17
      platformio
      
      wget
      unzip
      unrar
      picocom

      tmux
      vim
      helix

      tutanota-desktop
      telegram-desktop
    ];
  };

  security = {
    sudo.wheelNeedsPassword = false;
  };

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = "experimental-features = nix-command flakes";
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };

  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "23.05"; # Did you read the comment?
}

