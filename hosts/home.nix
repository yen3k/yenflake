{ config, lib, pkgs, user, ... }:
{
  home.username = "${user}";
  home.homeDirectory = "/home/${user}";

  programs.bash = {
    enable = true;
    enableCompletion = true;
  };

  programs.git = {
    enable = true;
    userName = "yen";
    userEmail = "git@yenmail.party";
    aliases = {
      st = "status -s";
      cm = "!git branch -d $(git branch --merged | sed -e s/\\*.*/\\ /g | tr -s \" \" | tr -s \"\\n\" \" \")";
    };
  };

  programs.thunderbird = {
    enable = true;
    profiles = {
      hiive = {
        isDefault = true;
      };
    };
  };

  nixpkgs.config.allowUnfree = true;

  xsession = {
    enable = true;
    numlock.enable = false;
  };

  home = {
    packages = with pkgs; [
      firefox
      pavucontrol
      vlc
      feh
      obs-studio
      audacity
    ];
  };

  home.stateVersion = "23.05";

  programs.home-manager.enable = true;
}
