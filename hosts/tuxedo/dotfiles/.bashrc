#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=100000

# Disable bell sound
#xset -b b off

#
# Aliases
#
alias ls='ls --color=auto'
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -lFh'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# start qdb without splash screen
alias gdb='gdb -q'

# xclip
alias xclip='xclip -selection clipboard'

# latex
alias latexclean='rm -r _minted-main *.{aux,log,toc,out,bcf,xml,bbl,blg,bib}'

# grep history
alias hg='history | grep'

PS1="\[\e[38;2;128;0;0m\][\u@\h]:\[\e[38;2;225;225;75m\]\w\$\[\e[0m\] "
export SSH_ASKPASS=""
