{ pkgs, ... }:
{
  home.packages = with pkgs; [ cowsay ];

  home.file.".Xresources".source = ./dotfiles/.Xresources;
  home.file.".Xdefaults".source = ./dotfiles/.Xdefaults;
  home.file.".bashrc".source = pkgs.lib.mkForce ./dotfiles/.bashrc;
  home.file.".config/i3/config".source = pkgs.lib.mkForce ./dotfiles/i3config;
  home.file.".config/i3/i3status_config.toml".source = pkgs.lib.mkForce ./dotfiles/i3status_config.toml;

}
